<?php
//                  _                   _   _     _
//             _ __| |_  _ __  __ _ _ _| |_(_)_ _(_)_ _ _  _ ___
//            | '_ \ ' \| '_ \/ _` | ' \  _| \ V / | '_| || (_-<
//            | .__/_||_| .__/\__,_|_||_\__|_|\_/|_|_|  \_,_/__/
//            |_|       |_|
//                               Version 1.0.3
//
//    Official Site:                                     Authors:
//    http://phpantivirus.sourceforge.net                KeyboardArtist
//                                                       Deekay
//    Sourceforge Page:                                  Nico
//    http://sourceforge.net/projects/phpantivirus/      Murphy
//
//    This software is provided as-is, without warranty or guarantee of
//    any kind. Use at your own risk. This software is licenced under the
//    GNU GPL license. More information is available in 'COPYING' included
//    with this distribution.

if (isset($_GET['scannow']) && $_GET['scannow'] == '1') {
    if (empty($_POST['scanpath']))
        header("Location: /scan.php");
}

// default configuration
$CONFIG = Array();
$CONFIG['extensions'] = Array();

// Example configuration file for PHP AntiVirus v1.0.3
// Please read INSTALL before editing this file.

// DEBUG MODE
// ----------
// Uncomment this option to enable 'debug' mode
// You will receive verbose reports including clean & infected
// files, as well as debug information for file reading and
// database connections.
// Default: Off (0)
if (isset($_GET['debug']) && $_GET['debug'] == '1') {
    $CONFIG['debug'] = 1;
} else {
    $CONFIG['debug'] = 0;
}

// ROOT PATH TO SCAN
// -----------------
// This can be a relative or full path WITHOUT a trailing
// slash. All files and folders will be recursively scanned
// within this path. NB: Due to your web host's configuration
// it is likely this script will be terminated after 30-60
// seconds of continuous operation. Please keep an eye on
// the number of files inside this directory - if it is too
// large it may fail.
// Default: Document root defined in Apache

//$CONFIG['scanpath'] = $_SERVER['DOCUMENT_ROOT'];

// SCANABLE FILES
// --------------
// The next few lines tell PHP AntiVirus what files to scan
// within the directory set above. It does it by file
// extension (the text after the period or dot in the file
// name) - for example "htm", "html" or "php" files.
// Default: None

// Static files? This should be a comprehensive list, add
// more if required.
$CONFIG['extensions'][] = 'htm';
$CONFIG['extensions'][] = 'html';
$CONFIG['extensions'][] = 'shtm';
$CONFIG['extensions'][] = 'shtml';
$CONFIG['extensions'][] = 'css';
$CONFIG['extensions'][] = 'js';
$CONFIG['extensions'][] = 'vbs';

// PHP files? This should be a comprehensive list, add more
// if required.
$CONFIG['extensions'][] = 'php';
$CONFIG['extensions'][] = 'php3';
$CONFIG['extensions'][] = 'php4';
$CONFIG['extensions'][] = 'php5';

// Text files? Virus code is harmless but invasive,
// although uncommenting these lines may cause false
// positives.
$CONFIG['extensions'][] = 'txt';
$CONFIG['extensions'][] = 'rtf';
$CONFIG['extensions'][] = 'doc';
$CONFIG['extensions'][] = 'docx';

// Flat file data? Only enable these if you regularly store
// data in flat files.
$CONFIG['extensions'][] = 'conf';
$CONFIG['extensions'][] = 'config';
$CONFIG['extensions'][] = 'csv';
$CONFIG['extensions'][] = 'tab';
$CONFIG['extensions'][] = 'sql';
$CONFIG['extensions'][] = 'dat';

// CGI scripts? Unlikely but entirely possible.
$CONFIG['extensions'][] = 'pl';
$CONFIG['extensions'][] = 'perl';
$CONFIG['extensions'][] = 'cgi';
// $CONFIG['extensions'][] = '';

// declare variables
$report = '';

// set counters
$dircount = 0;
$filecount = 0;
$infected = 0;

// load virus defs from flat file
//if (!check_defs('/virus.def'))
//	trigger_error("Virus.def vulnerable to overwrite, please change permissions", E_USER_ERROR);
$defs = load_defs('/virus.def', $CONFIG['debug']);

function file_scan($folder, $defs, $debug) {
    // hunts files/folders recursively for scannable items
	global $dircount, $report;
	$dircount++;
	if ($debug)
		$report .= "<p class=\"s\">Scanning folder $folder ...</p>";
	if ($d = dir($folder)) {
	    while (false !== ($entry = $d->read())) {
			$isdir = @is_dir($folder . '/' . $entry);
			if (!$isdir and $entry != '.' and $entry != '..') {
				virus_check($folder . '/' . $entry, $defs, $debug);
			} elseif ($isdir and $entry != '.' and $entry != '..') {
				file_scan($folder . '/' . $entry, $defs, $debug);
			}
		}
		$d->close();
	}
}

function virus_check($file, $defs, $debug) {
	global $filecount, $infected, $report, $CONFIG;

	// find scannable files
	$scannable = 0;
	foreach ($CONFIG['extensions'] as $ext) {
		if (substr($file, -3) == $ext)
			$scannable = 1;
	}

	// compare against defs
	if ($scannable) {
		// affectable formats
		$filecount++;
		$data = file($file);
		$data = implode('\r\n', $data);
		$clean = 1;
		foreach ($defs as $virus) {
			if (strpos($data, $virus[1])) {
				// file matches virus defs
				$report .= '<p class="r">Infected: ' . $file . ' (' . $virus[0] . ')</p>';
				$infected++;
				$clean = 0;
			}
		}
        if (eval_check($data)) {
            // file has eval code
            $report .= '<p class="r">EVAL Infected: ' . $file . '</p>';
            $infected++;
            $clean = 0;
        }
		if (($debug) && ($clean))
			$report .= '<p class="g">Clean: ' . $file . '</p>';
	}
}

function load_defs($file, $debug) {
	// reads tab-delimited defs file
	$defs = file($file);
	$counter = 0;
	$counttop = sizeof($defs);
	while ($counter < $counttop) {
		$defs[$counter] = explode('	', $defs[$counter]);
		$counter++;
	}
	return $defs;
}

function check_defs($file) {
	// check for > 755 perms on virus defs
	clearstatcache();
	$perms = substr(decoct(fileperms($file)), -2);
	if ($perms > 55)
		return false;
	else
		return true;
}

function eval_check($contents='') {
    if (preg_match('/eval\((base64|eval|\$_|\$\$|\$[A-Za-z_0-9\{]*(\(|\{|\[))/i', $contents)) {
        return true;
    }
}

/*function sendalert() {
    if (count($this->infected_files) != 0) {
        $message = "== MALICIOUS CODE FOUND == \n\n";
        $message .= "The following files appear to be infected: \n";
        foreach ($this->infected_files as $inf) {
            $message .= "  -  $inf \n";
        }
        mail(SEND_EMAIL_ALERTS_TO, 'Malicious Code Found!', $message, 'FROM:');
    }
}*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Server Information & Tools">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3>Scan Against Virus Definitions</h3>
            <div id="scanform">
                <form action="/scan.php?scannow=1" method="post">
                    <input type="text" name="scanpath" size="80" value="" placeholder="<?php echo getcwd(); ?>" />
                    <input type="submit" name="go" value="Scan" />
                </form>
            </div>
            <div id="scanformdebug">
                <form action="/scan.php?scannow=1&debug=1" method="post">
                    <input type="text" name="scanpath" size="80" value="" placeholder="<?php echo getcwd(); ?>" />
                    <input type="submit" name="go" value="Scan with DEBUG" />
                </form>
            </div>
            <?php if (isset($_GET['scannow']) && $_GET['scannow'] != '') { ?>
                <p><a href="/scan.php"><button>Start Over</button></a></p><br>
            <?php } ?>
            <?php
            if ($CONFIG['debug']) {
                echo '<div id="loaded">Loaded ' . sizeof($defs) . ' virus definitions</div>';
            }
            if (isset($_GET['scannow']) && $_GET['scannow'] == '1') {
                if (!empty($_POST['scanpath'])) {
                    $CONFIG['scanpath'] = $_POST['scanpath'];
                } else {
                    $CONFIG['scanpath'] = getcwd();
                }

                // scan specified root for specified defs
                file_scan($CONFIG['scanpath'], $defs, $CONFIG['debug']);

                // output summary
                echo '<div id="summary">';
                echo '<p><strong>Scanned folders:</strong> ' . $dircount . '</p>';
                echo '<p><strong>Scanned files:</strong> ' . $filecount . '</p>';
                echo '<p class="r"><strong>Infected files:</strong> ' . $infected . '</p>';
                echo '</div>';

                // output full report
                echo '<div id="report">';
                echo $report;
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>